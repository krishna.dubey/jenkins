Role Name = Gitlab
-------------

Installs GitLab, a Ruby-based front-end to Git, on any RedHat/CentOS or Debian/Ubuntu linux system.

Requirements 
---------
None



Role Variables
--------------
Available variables are listed below, along with default value

gitlab external url: "https://gitlab/"


Th URL at which  GitLab instance will be accessible.

Dependencies
------------
- curl
- openssh-server
- ca-certificates
- postfix 



